# Author: Adam Fuller
# Date: 20/5/2015
# Description: In Linux, read and parse mouse data.  Format information from
# http://wiki.osdev.org/Mouse_Input#Mouse_Packet_Info.

class listener():

	"""
	An approximately queue-like object.  Instantiate it and it begins listening
	for and accumulating mouse events.  Later, call its .get() method to
	retrieve all events since the last call to get() in a Pandas DataFrame.
	"""

	def __init__(self, df='/dev/input/mouse0', timestamp=None):

		"""
		Wraps a never-ending loop of _get() calls in a Thread and returns a Queue
		object.  timestamp should be one of [None, 'arrow', 'pandas', 'time'].
		"""

		import Queue
		import threading


		self.df = df

		self._stop=False


		try:

			import pandas as pd

			self.pandas = True

		except ImportError:

			import numpy as np

			self.pandas = False




		self._timestamp = timestamp

		if timestamp == 'arrow':

			import arrow

			ftime = arrow.now

		elif timestamp == 'pandas':

			import pandas

			ftime = pandas.Timestamp.now

		elif timestamp == 'time':

			import time

			ftime = time.time

		def _get():

			"""
			Returns contents of one mouse packet as a dict.
			"""

			def _parse_mouse_packet(p):

				"""
				Parse the contents of a standard mouse data packet.
				"""

				# Parse the first byte, which includes the signs of dx and dy.
				r = _parse_first_byte(p[0])

				# Apply the signs from the first byte to dx (second byte) and dy
				# (third byte).
				r.update({
				'dx': p[1] - {True: 1<<8, False: 0}[r['X negative']],
				'dy': p[2] - {True: 1<<8, False: 0}[r['Y negative']],
				})

				return r

			def _parse_first_byte(b):

				"""
				Parse the first byte of a standard mouse packet.
				"""

				v = map(lambda x: {'1': True, '0': False}[x], bin(b)[2:].zfill(8))

				k = [
					'Y overflow',
					'X overflow',
					'Y negative',
					'X negative',
					'Always 1',
					'Middle button',
					'Right button',
					'Left button',
					]
				
				return dict(zip(k,v))

			def _get_mouse_packet(df=self.df, b=3):

				"""
				Get 3 bytes from the mouse device file and interpret as as many
				integers.
				"""

				import struct

				# Open mouse device for reading.  Make a member of the class.
				self.d = open(df, 'rb')

				# Read b bytes from the device file.  This will block until b bytes
				# are available.  If no mouse events are occuring, the caller will
				# wait here.
				r = self.d.read(b)

				# Close mouse device file.
				self.d.close()

				# Interpret bytes as integers.
				return struct.unpack(b*'B', r)

			return _parse_mouse_packet(_get_mouse_packet())

		def get_and_queue(q):

			def call_get():

				r = _get()

				if timestamp:

					r.update({'Time': ftime()})

				if self.pandas:

					q.put(r)

				else:

					self.index = r.keys()
	
					q.put(r.values())

			while not self._stop:

				t = threading.Thread(target=call_get())

				t.start()

				t.join(timeout=1.0)

		self.q = Queue.Queue()

		self.t = threading.Thread(target=get_and_queue, args=(self.q,))

		self.t.start()
	
	def get(self):

		"""
		Returns contents of self.q, nicely formatted.
		"""

		if self.q.empty():
			
			return None

		else:


			try:

				import pandas as pd

				self.pandas = True

			except ImportError:

				import numpy as np

				self.pandas = False


			if self.pandas:

				if self._timestamp:

					r = pd.DataFrame(list(self.q.queue)).set_index('Time')

				else:

					r = pd.DataFrame(list(self.q.queue))

			else:

				ra = np.array(self.q.queue)

				rd = {}

				for e in range(ra.shape[1]):


					rd[self.index[e]] = ra[:,e]

				r = rd

			self.q.queue.clear()

			return r

	def stop(self):

		# Instruct the loop in get_and_queue to break.
		self._stop = True

		# Wait for the get_and_queue thread to finish.  This should happen on
		# the next packet since self._stop=True
		self.t.join()
