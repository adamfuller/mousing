# Description: #

A module for getting mouse data in IPython on Linux.

[osdev.org](http://wiki.osdev.org/Mouse_Input#Mouse_Packet_Info) provides a good reference on the mouse data packet format.

Currently quite heavy import requirements (e.g. pandas, matplotlib) relative to the intended Beaglebone Black target.  Only Queue and threading should be required.

A `stop` method cleanly releases the device file and closes the read thread *as long as there is at least one packet to read following the call*.

# To test: #

Read privileges are needed for `dev/input/mouse0` so either execute ipython as root:
```console
$ sudo ipython
```
or open up the device file's privileges:
```console
$ sudo chmod +r /dev/input/mouse0
$ ll /dev/input/mouse0
crw-r--r-- 1 root root 13, 32 May 13 10:32 /dev/input/mouse0
$ ipython
```

# To use: #

```python
In [2]: import mousing
```
Instantiate a listener, creating a [Queue](https://docs.python.org/2/library/queue.html) into which mouse packets are placed.
```python
In [3]: x = mousing.listener()
```

Now, do some mousing to cause some mouse events.  On the order of 20 per second will occur when the mouse is moving.

The first call to the `get` method returns mouse data since the listener was created in a Pandas DataFrame.  This also clears the listener's Queue.
```python
In [4]: x.get()
```

...even more mousing...

Subsequent calls to `get` retrieve mouse data since `get` was last called and clear the queue.

```python
In [5]: x.get().head()
```
```
Out[5]: 
                                 Always 1 Left button Middle button  \
Time                                                                  
2015-05-20T19:07:48.074454+12:00     True       False         False   
2015-05-20T19:07:48.088457+12:00     True       False         False   
2015-05-20T19:07:48.097906+12:00     True       False         False   
2015-05-20T19:07:48.120382+12:00     True       False         False   
2015-05-20T19:07:48.129920+12:00     True       False         False   

                                 Right button X negative X overflow  \
Time                                                                  
2015-05-20T19:07:48.074454+12:00        False       True      False   
2015-05-20T19:07:48.088457+12:00        False       True      False   
2015-05-20T19:07:48.097906+12:00        False       True      False   
2015-05-20T19:07:48.120382+12:00        False       True      False   
2015-05-20T19:07:48.129920+12:00        False       True      False   

                                 Y negative Y overflow  dx  dy  
Time                                                            
2015-05-20T19:07:48.074454+12:00      False      False  -2   0  
2015-05-20T19:07:48.088457+12:00       True      False  -6  -1  
2015-05-20T19:07:48.097906+12:00       True      False  -8  -3  
2015-05-20T19:07:48.120382+12:00       True      False  -4  -7  
2015-05-20T19:07:48.129920+12:00       True      False  -4  -4  

[5 rows x 10 columns]
```

Calling x.get() before any more mouse events returns None.
```python
In [6]: x.get()

In [7]: 
```

Call the stop method then move the mouse one last time to allow the device file to close.  Without this, attempting to exit ipython will hang.
```python
In[8]: x.stop()
```